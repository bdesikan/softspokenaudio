## Tube Howler Distortion VST plugin
---
Development Notes:

1. Created the Distortion plugin using tanh based function
2. TODO
	- 2a. Add config files handling
	- 2b. perform Error handling
	- 2c. Implement other Waveshaper function apart from tanh and
	    add this to the config file as parameter
	- 2d. Perform oversampling and check its improvement on quality
	- 2e. "Octave" Pitch shifting implementation

3. This google doc will contain the status meeting updates.
   [status doc](https://docs.google.com/document/d/17lHvNK2kas3UobFC035tccChMxUmhJwdRXnCCVbW5w4/edit?usp=sharing)

---

## General Future refernce note
 
1. Check the document under "literature survey" for general documentation


---

## Usage of the Plugin with DAW

1. Please use the optimal settings for the knobs of the plugin in Reaper environment as shown in the below screen shot.

![image](https://drive.google.com/uc?export=view&id=1GL--ARCVZrwfnf2OslkFT2_Pf5zcuD1j)

Copyright &copy; [2021] [Bharadwaj Desikan]
