/*
  ==============================================================================

    Identifiers.h
    Created: 12 Mar 2019 10:34:42am
    Author:  Joonas

  ==============================================================================
*/

#pragma once

namespace IDs {

	#define DECLARE_ID(name) const String name (#name);

	DECLARE_ID(gainDrive)			// Input volume of the effects chain
	DECLARE_ID(LPFreq)				// Frequency of the lowpass filter after the waveshaper	
	DECLARE_ID(EffectVolume)		// Output volume of the effects chain
	DECLARE_ID(pitch)				// shifts the output by 12 semitones higher or lower

	#undef DECLARE_ID

}