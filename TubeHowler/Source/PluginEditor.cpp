/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
SoftSpokenAudioAudioProcessorEditor::SoftSpokenAudioAudioProcessorEditor (SoftSpokenAudioAudioProcessor& p)
    : AudioProcessorEditor (&p), audioProcessor (p), mParameter(p.getState())
{

	setSize (254, 430);

	// GAIN DRIVE ================================
	// Label
	mgainDriveLabel.setText("DRIVE", dontSendNotification);
	mgainDriveLabel.setJustificationType(Justification::centred);
	addAndMakeVisible(mgainDriveLabel);
	// Slider
	mgainDriveSlider.setSliderStyle(Slider::SliderStyle::Rotary);
	mgainDriveSlider.setTextBoxStyle(Slider::TextBoxBelow, false, mTextBoxWidth, mTextBoxHeight);

	mgainDriveSlider.setTextValueSuffix(" dB");
	mgainDriveAttachment.reset(new SliderAttachment(mParameter, IDs::gainDrive, mgainDriveSlider));
	addAndMakeVisible(mgainDriveSlider);
	mgainDriveSlider.setLookAndFeel(&mgainDriveKnobLAF);
	
	// Tone FILTER =================================
	// Label
	mToneLabel.setText("TONE", dontSendNotification);
	mToneLabel.setJustificationType(Justification::centred);
	addAndMakeVisible(mToneLabel);
	// Slider
	mToneSlider.setSliderStyle(Slider::SliderStyle::Rotary);
	mToneSlider.setTextBoxStyle(Slider::TextBoxBelow, false, mTextBoxWidth, mTextBoxHeight);
	mToneSlider.setTextValueSuffix(" Hz");
	mToneAttachment.reset(new SliderAttachment(mParameter, IDs::LPFreq, mToneSlider));
	addAndMakeVisible(mToneSlider);
	mToneSlider.setLookAndFeel(&mgainDriveKnobLAF);

	// EffectVolume =================================
	// Label
	mEffectVolumeLabel.setText("LEVEL", dontSendNotification);
	mEffectVolumeLabel.setJustificationType(Justification::centred);
	addAndMakeVisible(mToneLabel);
	// Slider
	mEffectVolumeSlider.setSliderStyle(Slider::SliderStyle::Rotary);
	mEffectVolumeSlider.setTextBoxStyle(Slider::TextBoxBelow, false, mTextBoxWidth, mTextBoxHeight);
	mEffectVolumeSlider.setTextValueSuffix(" dB");
	mEffectVolumeAttachment.reset(new SliderAttachment(mParameter, IDs::EffectVolume, mEffectVolumeSlider));
	addAndMakeVisible(mEffectVolumeSlider);
	mEffectVolumeSlider.setLookAndFeel(&mgainDriveKnobLAF);

	// PITCH =================================
	// Label
	mpitchLabel.setText("OCTAVE", dontSendNotification);
	mpitchLabel.setJustificationType(Justification::centred);
	addAndMakeVisible(mToneLabel);
	// Slider
	mpitchSlider.setSliderStyle(Slider::SliderStyle::Rotary);
	mpitchSlider.setTextBoxStyle(Slider::TextBoxBelow, false, mTextBoxWidth, mTextBoxHeight);
	mpitchAttachment.reset(new SliderAttachment(mParameter, IDs::pitch, mpitchSlider));
	addAndMakeVisible(mpitchSlider);
	mpitchSlider.setLookAndFeel(&mgainDriveKnobLAF);
	
	mPedalSw.setImages(true, true, true,
		ImageCache::getFromMemory(BinaryData::footswitch_up_png, BinaryData::footswitch_up_pngSize), 1.0, Colours::transparentWhite,
		Image(), 1.0, Colours::transparentWhite,
		ImageCache::getFromMemory(BinaryData::footswitch_down_png, BinaryData::footswitch_down_pngSize), 1.0, Colours::transparentWhite,
		0.0);
	addAndMakeVisible(mPedalSw);
	mPedalSw.addListener(this);
	mPedalSw.setClickingTogglesState(true);

	mStatusLED.setImages(true, true, true,
		ImageCache::getFromMemory(BinaryData::led_red_off_png, BinaryData::led_red_off_pngSize), 1.0, Colours::transparentWhite,
		Image(), 1.0, Colours::transparentWhite,
		ImageCache::getFromMemory(BinaryData::led_red_on_png, BinaryData::led_red_on_pngSize), 1.0, Colours::transparentWhite,
		0.0);
	addAndMakeVisible(mStatusLED);
	mStatusLED.setClickingTogglesState(true);

	mPedalSw.setToggleState(true, dontSendNotification);
	mStatusLED.setToggleState(true, dontSendNotification);

	// Set Widget Graphics
    mgainDriveKnobLAF.setLookAndFeel(ImageCache::getFromMemory(BinaryData::knob_hex_png, BinaryData::knob_hex_pngSize));

}

SoftSpokenAudioAudioProcessorEditor::~SoftSpokenAudioAudioProcessorEditor()
{
	
	mgainDriveSlider.setLookAndFeel(nullptr);
    mToneSlider.setLookAndFeel(nullptr);
    mpitchSlider.setLookAndFeel(nullptr);
	mEffectVolumeSlider.setLookAndFeel(nullptr);

}

//==============================================================================
void SoftSpokenAudioAudioProcessorEditor::paint (juce::Graphics& g)
{
    // (Our component is opaque, so we must completely fill the background with a solid colour)
    g.fillAll (getLookAndFeel().findColour (juce::ResizableWindow::backgroundColourId));
	g.setColour(Colours::white);
	g.setFont(10.0f);
    background = ImageCache::getFromMemory(BinaryData::BGImage_png, BinaryData::BGImage_pngSize);

    g.drawImageAt(background, 0, 0);


	// Status handling LED
	// Should really override the ToggleButton class, but being lazy here
	/*if (audioProcessor.mPedalStatus == 1) {
		mStatusLED.setImages(true, true, true,
			ImageCache::getFromMemory(BinaryData::led_red_on_png, BinaryData::led_red_on_pngSize), 1.0, Colours::transparentWhite,
			Image(), 1.0, Colours::transparentWhite,
			ImageCache::getFromMemory(BinaryData::led_red_on_png, BinaryData::led_red_on_pngSize), 1.0, Colours::transparentWhite,
			0.0);
	}
	else {
		mStatusLED.setImages(true, true, true,
			ImageCache::getFromMemory(BinaryData::led_red_off_png, BinaryData::led_red_off_pngSize), 1.0, Colours::transparentWhite,
			Image(), 1.0, Colours::transparentWhite,
			ImageCache::getFromMemory(BinaryData::led_red_off_png, BinaryData::led_red_off_pngSize), 1.0, Colours::transparentWhite,
			0.0);
	}*/

}

void SoftSpokenAudioAudioProcessorEditor::resized()
{
    // This is generally where you'll want to lay out the positions of any
    // subcomponents in your editor..
	

    // Overdrive Widgets
	mgainDriveSlider.setBounds(30, 25, 85, 85);
	mEffectVolumeSlider.setBounds(135, 25, 85, 85);
	mpitchSlider.setBounds(30, 115, 85, 85);
	mToneSlider.setBounds(135, 115, 85, 85);
	mPedalSw.setBounds(90, 350, 100, 100);
	mStatusLED.setBounds(107, 92, 75, 75);
    /*auto buttonHeight = 30;

    button1.setBounds(area.removeFromTop(buttonHeight).reduced(border));
    button2.setBounds(area.removeFromTop(buttonHeight).reduced(border));*/

}

void SoftSpokenAudioAudioProcessorEditor::mPedalSwClicked() {
	if (audioProcessor.mPedalStatus == 0)
	{
		audioProcessor.mPedalStatus = 1;
		mPedalSw.setToggleState(true, dontSendNotification);
		mStatusLED.setToggleState(true, dontSendNotification);
	}
	else
	{
		audioProcessor.mPedalStatus = 0;
		mPedalSw.setToggleState(false, dontSendNotification);
		mStatusLED.setToggleState(false, dontSendNotification);
	}
}

void SoftSpokenAudioAudioProcessorEditor::buttonClicked(juce::Button* button)
{
	if (button == &mPedalSw) {
		mPedalSwClicked();
	}
	/*else if (button == &loadButton) {
		loadButtonClicked();
	}*/
}