/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include "PluginProcessor.h"
#include "myLookAndFeel.h"
#include "Identifiers.h"
//==============================================================================
/**
*/
class SoftSpokenAudioAudioProcessorEditor  : public AudioProcessorEditor, private Button::Listener
{
public:
    SoftSpokenAudioAudioProcessorEditor (SoftSpokenAudioAudioProcessor&);
    ~SoftSpokenAudioAudioProcessorEditor() override;

    //==============================================================================
    void paint (juce::Graphics&) override;
    void resized() override;

private:
    // This reference is provided as a quick way for your editor to
    // access the processor object that created it.
    SoftSpokenAudioAudioProcessor& audioProcessor;
    AudioProcessorValueTreeState& mParameter;

    typedef AudioProcessorValueTreeState::SliderAttachment SliderAttachment;

    // Sliders
	Slider mgainDriveSlider;
	Slider mToneSlider;
    Slider mEffectVolumeSlider;
	Slider mpitchSlider;

	// Labels
	Label mgainDriveLabel;
	Label mToneLabel;
    Label mEffectVolumeLabel;
	Label mpitchLabel;

    // LAF 
    //KnobLookAndFeel knobLookAndFeel;
	myLookAndFeel mgainDriveKnobLAF;

	
	Image background;
    
    // pedal and Led
    ImageButton mPedalSw;
    ImageButton mStatusLED;
    virtual void buttonClicked(Button* button) override;
    void mPedalSwClicked();

	int mSliderSize = 80;
	int mTextBoxHeight = 0;
	int mTextBoxWidth = 0;

	std::unique_ptr<SliderAttachment> mgainDriveAttachment;
	std::unique_ptr<SliderAttachment> mToneAttachment;
    std::unique_ptr<SliderAttachment> mEffectVolumeAttachment;
	std::unique_ptr<SliderAttachment> mpitchAttachment;

    //juce::ToggleButton m_BypassButton;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (SoftSpokenAudioAudioProcessorEditor)
};

