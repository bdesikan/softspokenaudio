/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include "PluginParameter.h"


/** Helper Macros

    All audio samples are represented in unit voltage (uV) as a float in the
    range of -1.0 to 1.0. To convert an audio sample into a decibel (dB) value
    pass the sample into `dB(x)`. The returned output can be used for audio
    analysis and dynamic procesing. To convert a decibel value back to unit
    voltage for output, pass the value into `uV(x)`. Do not output your samples
    in decibels.

    It is important to keep track of when your values are in decibels or
    unit voltage. Be sure to label your variables accordingly.
*/
#define dB(x) 20.0 * ((x) > 0.00001 ? log10(x) : -5.0)  // uV -> dB
#define uV(x) pow(10.0, (x) / 20.0)                     // dB -> uV

//==============================================================================
/**
*/
class SoftSpokenAudioAudioProcessor  : public juce::AudioProcessor
{
public:
    //==============================================================================
    SoftSpokenAudioAudioProcessor();
    ~SoftSpokenAudioAudioProcessor() override;

    //==============================================================================
    void prepareToPlay (double sampleRate, int samplesPerBlock) override;
    void releaseResources() override;

   #ifndef JucePlugin_PreferredChannelConfigurations
    bool isBusesLayoutSupported (const BusesLayout& layouts) const override;
   #endif

    void processBlock (juce::AudioBuffer<float>&, juce::MidiBuffer&) override;

    //==============================================================================
    juce::AudioProcessorEditor* createEditor() override;
    bool hasEditor() const override;

    //==============================================================================
    const juce::String getName() const override;

    bool acceptsMidi() const override;
    bool producesMidi() const override;
    bool isMidiEffect() const override;
    double getTailLengthSeconds() const override;

    //==============================================================================
    int getNumPrograms() override;
    int getCurrentProgram() override;
    void setCurrentProgram (int index) override;
    const juce::String getProgramName (int index) override;
    void changeProgramName (int index, const juce::String& newName) override;

    //==============================================================================
    void getStateInformation (juce::MemoryBlock& destData) override;
    void setStateInformation (const void* data, int sizeInBytes) override;

	void reset();
	void updateParameters();
	AudioProcessorValueTreeState& getState();

    // Pedal state
    int mPedalStatus = 1;       // 0 = off, 1 = on
	
private:

    AudioProcessorValueTreeState mParameters;
	static constexpr size_t numWaveShapers = 1;
	dsp::WaveShaper<float> mWaveShapers[numWaveShapers];
	dsp::ProcessorDuplicator<dsp::IIR::Filter<float>, dsp::IIR::Coefficients<float>> mLowPassFilter;
    dsp::ProcessorDuplicator<dsp::IIR::Filter<float>, dsp::IIR::Coefficients<float>> mHighPassFilter;
    std::unique_ptr<dsp::Oversampling<float>> mOversampling;
	dsp::Gain<float> mgainDrive, mEffectVolume, mToneBoost;
	
	
	// Filter Definition
    IIRFilter lowBandFilterL1,lowBandFilterL2,lowBandFilterR1,lowBandFilterR2;
    IIRFilter lowMidBandFilterL1,lowMidBandFilterL2, lowMidBandFilterR1,lowMidBandFilterR2;
    IIRFilter highMidBandFilterL1, highMidBandFilterL2, highMidBandFilterR1, highMidBandFilterR2;
    IIRFilter highBandFilterL1, highBandFilterL2, highBandFilterR1, highBandFilterR2;
    IIRCoefficients coefficients;
    
    // Cutoff Frequencies
    float pLowPassCutoff;
    float pHighPassCutoff;
	
	// Parameters
    float pOverallGain;
    float pToneBoost;


	float mSampleRate = 48000.f;
	uint32 mMaxBlockSize = 512;
	uint32 mNumChannels = 2;
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (SoftSpokenAudioAudioProcessor)
};
