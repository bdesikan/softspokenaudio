/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
SoftSpokenAudioAudioProcessor::SoftSpokenAudioAudioProcessor()
#ifndef JucePlugin_PreferredChannelConfigurations
     : AudioProcessor (BusesProperties()
                     #if ! JucePlugin_IsMidiEffect
                      #if ! JucePlugin_IsSynth
                       .withInput  ("Input",  juce::AudioChannelSet::stereo(), true)
                      #endif
                       .withOutput ("Output", juce::AudioChannelSet::stereo(), true)
                     #endif
                       ),
	   mParameters(*this, nullptr, Identifier("SoftSpokenAudioValueTree"),
	   {
			  std::make_unique<AudioParameterFloat>(IDs::gainDrive,
													"DRIVE",
													NormalisableRange<float>(20.f, 80.f),
													0.f,
													"dB",
													AudioProcessorParameter::genericParameter,
													[](float value, int) {return static_cast<String>(round(value * 100.f) / 100.f); },
													nullptr
													),
			  std::make_unique<AudioParameterFloat>(IDs::EffectVolume,
													"LEVEL",
													NormalisableRange<float>(-100.f, 10.f),
													0.f,
													"dB",
													AudioProcessorParameter::genericParameter,
													[](float value, int) {return static_cast<String>(round(value * 100.f) / 100.f); },
													nullptr
													),
			  std::make_unique<AudioParameterFloat>(IDs::LPFreq,
													"TONE",
													NormalisableRange<float>(0, 20.f),
													0.f,
													"dB",
													AudioProcessorParameter::genericParameter,
													[](float value, int) {return static_cast<String>(round(value * 100.f) / 100.f); },
													nullptr
													),
			  std::make_unique<AudioParameterFloat>(IDs::pitch,
													"OCTAVE",
													NormalisableRange<float>(-12.f, 12.f, 12.0f),
													0.0,
													String(),
													AudioProcessorParameter::genericParameter,
													[](float value, int) {return static_cast<String>(round(value * 100.f * 100.f) / 100.f); },
													nullptr
													) 
		}),
		mWaveShapers{ { std::atan } },
		mLowPassFilter(dsp::IIR::Coefficients<float>::makeFirstOrderLowPass(48000.f, 24000.f)),
		mHighPassFilter(dsp::IIR::Coefficients<float>::makeFirstOrderHighPass(48000.f, 20.f))
#endif
{
	// Default Cutoff Frequencies
    pLowPassCutoff = 100;
    pHighPassCutoff = 4000;
	mOversampling.reset(new dsp::Oversampling<float>(2, 3, dsp::Oversampling<float>::filterHalfBandPolyphaseIIR, false));
}

SoftSpokenAudioAudioProcessor::~SoftSpokenAudioAudioProcessor()
{
	mOversampling = nullptr;
}

//==============================================================================
const juce::String SoftSpokenAudioAudioProcessor::getName() const
{
    return JucePlugin_Name;
}

bool SoftSpokenAudioAudioProcessor::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
    return true;
   #else
    return false;
   #endif
}

bool SoftSpokenAudioAudioProcessor::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}

bool SoftSpokenAudioAudioProcessor::isMidiEffect() const
{
   #if JucePlugin_IsMidiEffect
    return true;
   #else
    return false;
   #endif
}

double SoftSpokenAudioAudioProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

int SoftSpokenAudioAudioProcessor::getNumPrograms()
{
    return 1;   // NB: some hosts don't cope very well if you tell them there are 0 programs,
                // so this should be at least 1, even if you're not really implementing programs.
}

int SoftSpokenAudioAudioProcessor::getCurrentProgram()
{
    return 0;
}

void SoftSpokenAudioAudioProcessor::setCurrentProgram (int index)
{
}

const juce::String SoftSpokenAudioAudioProcessor::getProgramName (int index)
{
    return {};
}

void SoftSpokenAudioAudioProcessor::changeProgramName (int index, const juce::String& newName)
{
}

//==============================================================================
void SoftSpokenAudioAudioProcessor::prepareToPlay (double sampleRate, int samplesPerBlock)
{
    // Use this method as the place to do any pre-playback
    // initialisation that you need..
    auto inputdB = Decibels::decibelsToGain(60);
	
	dsp::ProcessSpec spec;
	spec.sampleRate = sampleRate;
	spec.maximumBlockSize = samplesPerBlock;
	spec.numChannels = getTotalNumOutputChannels();

	// Initialize class variables
	mSampleRate = static_cast<float>(spec.sampleRate);
	mMaxBlockSize = spec.maximumBlockSize;
	mNumChannels = spec.numChannels;
	// Prepare
	mgainDrive.prepare(spec);
	mEffectVolume.prepare(spec);
	mLowPassFilter.prepare(spec);
	mHighPassFilter.prepare(spec);
	mOversampling->initProcessing(static_cast<size_t> (mMaxBlockSize));

    // Calculate Filter Coefficients
    //==============================
    // First Left Channel Filters
    lowBandFilterL1.setCoefficients(coefficients.makeLowPass(sampleRate, pLowPassCutoff));
    lowMidBandFilterL1.setCoefficients(coefficients.makeHighPass(sampleRate, pLowPassCutoff));
    highMidBandFilterL1.setCoefficients(coefficients.makeLowPass(sampleRate, pHighPassCutoff));
    highBandFilterL1.setCoefficients(coefficients.makeHighPass(sampleRate, pHighPassCutoff));

    // First Right Channel Filters
    lowBandFilterR1.setCoefficients(coefficients.makeLowPass(sampleRate, pLowPassCutoff));
    lowMidBandFilterR1.setCoefficients(coefficients.makeHighPass(sampleRate, pLowPassCutoff));
    highMidBandFilterR1.setCoefficients(coefficients.makeLowPass(sampleRate, pHighPassCutoff));
    highBandFilterR1.setCoefficients(coefficients.makeHighPass(sampleRate, pHighPassCutoff));

    // Second Left Channel Filters
    lowBandFilterL2.setCoefficients(coefficients.makeLowPass(sampleRate, pLowPassCutoff));
    lowMidBandFilterL2.setCoefficients(coefficients.makeHighPass(sampleRate, pLowPassCutoff));
    highMidBandFilterL2.setCoefficients(coefficients.makeLowPass(sampleRate, pHighPassCutoff));
    highBandFilterL2.setCoefficients(coefficients.makeHighPass(sampleRate, pHighPassCutoff));

    // Second Right Channel Filters
    lowBandFilterR2.setCoefficients(coefficients.makeLowPass(sampleRate, pLowPassCutoff));
    lowMidBandFilterR2.setCoefficients(coefficients.makeHighPass(sampleRate, pLowPassCutoff));
    highMidBandFilterR2.setCoefficients(coefficients.makeLowPass(sampleRate, pHighPassCutoff));
    highBandFilterR2.setCoefficients(coefficients.makeHighPass(sampleRate, pHighPassCutoff));

	reset();

}

void SoftSpokenAudioAudioProcessor::reset()
{
	mOversampling->reset();
	mLowPassFilter.reset();
	mHighPassFilter.reset();
}

void SoftSpokenAudioAudioProcessor::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}

#ifndef JucePlugin_PreferredChannelConfigurations
bool SoftSpokenAudioAudioProcessor::isBusesLayoutSupported (const BusesLayout& layouts) const
{
  #if JucePlugin_IsMidiEffect
    juce::ignoreUnused (layouts);
    return true;
  #else
    // This is the place where you check if the layout is supported.
    // In this template code we only support mono or stereo.
    if (layouts.getMainOutputChannelSet() != juce::AudioChannelSet::mono()
     && layouts.getMainOutputChannelSet() != juce::AudioChannelSet::stereo())
        return false;

    // This checks if the input layout matches the output layout
   #if ! JucePlugin_IsSynth
    if (layouts.getMainOutputChannelSet() != layouts.getMainInputChannelSet())
        return false;
   #endif

    return true;
  #endif
}
#endif

void SoftSpokenAudioAudioProcessor::processBlock (juce::AudioBuffer<float>& buffer, juce::MidiBuffer& midiMessages)
{
    juce::ScopedNoDenormals noDenormals;
    auto totalNumInputChannels  = getTotalNumInputChannels();
    auto totalNumOutputChannels = getTotalNumOutputChannels();

    // In case we have more outputs than inputs, this code clears any output
    // channels that didn't contain input data, (because these aren't
    // guaranteed to be empty - they may contain garbage).
    // This is here to avoid people getting screaming feedback
    // when they first compile a plugin, but obviously you don't need to keep
    // this code if your algorithm always overwrites all the output channels
     
    float sampleRate = getSampleRate();

	// Checking Pedal status ================================================================== 
	if (mPedalStatus == 1) {
		
		auto numSamples = buffer.getNumSamples();

		for (auto i = jmin(2, totalNumInputChannels); i < totalNumOutputChannels; ++i)
			buffer.clear(i, 0, numSamples);

		updateParameters();

		dsp::AudioBlock<float> block(buffer);

		if (block.getNumChannels() > 2)
			block = block.getSubsetChannelBlock(0, 2);
		dsp::ProcessContextReplacing<float> context = dsp::ProcessContextReplacing<float>(block);
		mgainDrive.process(context);

		//mHighPassFilter.process(context);

		dsp::AudioBlock<float> oversampledBlock  = mOversampling -> processSamplesUp(context.getInputBlock());
		auto waveshaperContext = context;

		mWaveShapers[0].process(context);
		context.getOutputBlock() *= 0.7f;

		// downsample
		mOversampling->processSamplesDown(context.getOutputBlock());

		mLowPassFilter.process(context);
		
		// Processing Xover fot tone control 

    // Recalculate the coefficients in case the cutoffs are altered
    // First Left Channel Filters
        lowBandFilterL1.setCoefficients(coefficients.makeLowPass(sampleRate, pLowPassCutoff));
        lowMidBandFilterL1.setCoefficients(coefficients.makeHighPass(sampleRate, pLowPassCutoff));
        highMidBandFilterL1.setCoefficients(coefficients.makeLowPass(sampleRate, pHighPassCutoff));
        highBandFilterL1.setCoefficients(coefficients.makeHighPass(sampleRate, pHighPassCutoff));

        // First Right Channel Filters
        lowBandFilterR1.setCoefficients(coefficients.makeLowPass(sampleRate, pLowPassCutoff));
        lowMidBandFilterR1.setCoefficients(coefficients.makeHighPass(sampleRate, pLowPassCutoff));
        highMidBandFilterR1.setCoefficients(coefficients.makeLowPass(sampleRate, pHighPassCutoff));
        highBandFilterR1.setCoefficients(coefficients.makeHighPass(sampleRate, pHighPassCutoff));

        // Second Left Channel Filters
        lowBandFilterL2.setCoefficients(coefficients.makeLowPass(sampleRate, pLowPassCutoff));
        lowMidBandFilterL2.setCoefficients(coefficients.makeHighPass(sampleRate, pLowPassCutoff));
        highMidBandFilterL2.setCoefficients(coefficients.makeLowPass(sampleRate, pHighPassCutoff));
        highBandFilterL2.setCoefficients(coefficients.makeHighPass(sampleRate, pHighPassCutoff));

        // Second Right Channel Filters
        lowBandFilterR2.setCoefficients(coefficients.makeLowPass(sampleRate, pLowPassCutoff));
        lowMidBandFilterR2.setCoefficients(coefficients.makeHighPass(sampleRate, pLowPassCutoff));
        highMidBandFilterR2.setCoefficients(coefficients.makeLowPass(sampleRate, pHighPassCutoff));
        highBandFilterR2.setCoefficients(coefficients.makeHighPass(sampleRate, pHighPassCutoff));



		// Initialise Buffer for Each Band
		dsp::AudioBlock<float> lowOutput(context.getOutputBlock());
		dsp::AudioBlock<float> midOutput(context.getOutputBlock());
		dsp::AudioBlock<float> highOutput(context.getOutputBlock());
			

		AudioBuffer<float> lowOutputBuf(lowOutput.getNumChannels(), lowOutput.getNumSamples());
		lowOutput.copyTo(lowOutputBuf);
		AudioBuffer<float> midOutputBuf(midOutput.getNumChannels(), midOutput.getNumSamples());
		midOutput.copyTo(midOutputBuf);
		AudioBuffer<float> highOutputBuf(highOutput.getNumChannels(), highOutput.getNumSamples());
		highOutput.copyTo(highOutputBuf);

		// Apply Filter onto the buffer
		//==============================
		// First Low Band Filtering Stage
		lowBandFilterL1.processSamples(lowOutputBuf.getWritePointer(0), numSamples);
		lowBandFilterR1.processSamples(lowOutputBuf.getWritePointer(1), numSamples);
		// Second Low Band Filtering Stage
		lowBandFilterL2.processSamples(lowOutputBuf.getWritePointer(0), numSamples);
		lowBandFilterR2.processSamples(lowOutputBuf.getWritePointer(1), numSamples);

		// First Low - Mid Band Filtering Stage
		lowMidBandFilterL1.processSamples(midOutputBuf.getWritePointer(0), numSamples);
		lowMidBandFilterR1.processSamples(midOutputBuf.getWritePointer(1), numSamples);
		// Second Low - Mid Band Filtering Stage
		lowMidBandFilterL2.processSamples(midOutputBuf.getWritePointer(0), numSamples);
		lowMidBandFilterR2.processSamples(midOutputBuf.getWritePointer(1), numSamples);

		//First High - Mid Band Filtering Stage
		highMidBandFilterL1.processSamples(midOutputBuf.getWritePointer(0), numSamples);
		highMidBandFilterR1.processSamples(midOutputBuf.getWritePointer(1), numSamples);
		// Second High - Mid Band Filtering Stage
		highMidBandFilterL2.processSamples(midOutputBuf.getWritePointer(0), numSamples);
		highMidBandFilterR2.processSamples(midOutputBuf.getWritePointer(1), numSamples);

		// First High Band Filtering Stage
		highBandFilterL1.processSamples(highOutputBuf.getWritePointer(0), numSamples);
		highBandFilterR1.processSamples(highOutputBuf.getWritePointer(1), numSamples);
		// Second High Band Filtering Stage
		highBandFilterL2.processSamples(highOutputBuf.getWritePointer(0), numSamples);
		highBandFilterR2.processSamples(highOutputBuf.getWritePointer(1), numSamples);


		midOutputBuf.applyGain(pToneBoost);

		// Sum Each Band
		buffer.clear();
		for (int channel = 0; channel < totalNumInputChannels; channel++)
		{
			buffer.addFrom(channel, 0, lowOutputBuf, channel, 0, numSamples, 1.0/3.0);
			buffer.addFrom(channel, 0, midOutputBuf, channel, 0, numSamples, 1.0/3.0);
			buffer.addFrom(channel, 0, highOutputBuf, channel, 0, numSamples, 1.0/3.0);
		}
		
		// Apply the Overall Gain
        // Apply the Overall Gain
        buffer.applyGain(pOverallGain);

		//mEffectVolume.process(context);
	}
	else
	{
		for (int ch = 1; ch < buffer.getNumChannels(); ++ch)
			buffer.copyFrom(ch, 0, buffer, 0, 0, buffer.getNumSamples());
	}
}

//==============================================================================
bool SoftSpokenAudioAudioProcessor::hasEditor() const
{
    return true; // (change this to false if you choose to not supply an editor)
}

juce::AudioProcessorEditor* SoftSpokenAudioAudioProcessor::createEditor()
{
    return new SoftSpokenAudioAudioProcessorEditor (*this);
}

//==============================================================================
void SoftSpokenAudioAudioProcessor::getStateInformation (juce::MemoryBlock& destData)
{
    // You should use this method to store your parameters in the memory block.
    // You could do that either as raw data, or use the XML or ValueTree classes
    // as intermediaries to make it easy to save and load complex data.
	auto state = mParameters.copyState();
	std::unique_ptr<XmlElement> xml(state.createXml());
	copyXmlToBinary(*xml, destData);
}

void SoftSpokenAudioAudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
    // You should use this method to restore your parameters from this memory block,
    // whose contents will have been created by the getStateInformation() call.
	std::unique_ptr<XmlElement> xmlState(getXmlFromBinary(data, sizeInBytes));

	if (xmlState.get() != nullptr)
		if (xmlState->hasTagName(mParameters.state.getType()))
			mParameters.replaceState(ValueTree::fromXml(*xmlState));
}

void SoftSpokenAudioAudioProcessor::updateParameters()
{
	float gainDrive = *mParameters.getRawParameterValue(IDs::gainDrive);
	float EffectVolume = *mParameters.getRawParameterValue(IDs::EffectVolume);
	float ToneBoost = *mParameters.getRawParameterValue(IDs::LPFreq);

	auto inputdB = Decibels::decibelsToGain(gainDrive);
	auto outputdB = Decibels::decibelsToGain(EffectVolume+6.0);
	pOverallGain = outputdB;
	auto TonedB = Decibels::decibelsToGain(ToneBoost); 
	pToneBoost = TonedB;

	if (mgainDrive.getGainLinear() != inputdB)     mgainDrive.setGainLinear(inputdB);
	if (mEffectVolume.getGainLinear() != outputdB)   mEffectVolume.setGainLinear(outputdB);
	if (mToneBoost.getGainLinear() != TonedB)   mToneBoost.setGainLinear(TonedB);
	
	//*mLowPassFilter.state = *dsp::IIR::Coefficients<float>::makeFirstOrderLowPass(mSampleRate, freqLowPass);
	/*float freqHighPass = *mParameters.getRawParameterValue(IDs::HPFreq);
	*mHighPassFilter.state = *dsp::IIR::Coefficients<float>::makeHighPass(mSampleRate, freqHighPass);*/
}

AudioProcessorValueTreeState& SoftSpokenAudioAudioProcessor::getState()
{
	return mParameters;
}

//==============================================================================
// This creates new instances of the plugin..
juce::AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new SoftSpokenAudioAudioProcessor();
}
