/* =========================================================================================

   This is an auto-generated file: Any edits you make may be overwritten!

*/

#pragma once

namespace BinaryData
{
    extern const char*   BGImage_png;
    const int            BGImage_pngSize = 69175;

    extern const char*   footswitch_down_png;
    const int            footswitch_down_pngSize = 8225;

    extern const char*   footswitch_up_png;
    const int            footswitch_up_pngSize = 7791;

    extern const char*   knob_hex_png;
    const int            knob_hex_pngSize = 230289;

    extern const char*   led_red_off_png;
    const int            led_red_off_pngSize = 3129;

    extern const char*   led_red_on_png;
    const int            led_red_on_pngSize = 3374;

    // Number of elements in the namedResourceList and originalFileNames arrays.
    const int namedResourceListSize = 6;

    // Points to the start of a list of resource names.
    extern const char* namedResourceList[];

    // Points to the start of a list of resource filenames.
    extern const char* originalFilenames[];

    // If you provide the name of one of the binary resource variables above, this function will
    // return the corresponding data and its size (or a null pointer if the name isn't found).
    const char* getNamedResource (const char* resourceNameUTF8, int& dataSizeInBytes);

    // If you provide the name of one of the binary resource variables above, this function will
    // return the corresponding original, non-mangled filename (or a null pointer if the name isn't found).
    const char* getNamedResourceOriginalFilename (const char* resourceNameUTF8);
}
